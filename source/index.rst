
Spatial Data Analysis and Modeling with R
=========================================

This website provides materials to learn about spatial data analysis and modeling with *R*. *R* is a widely used programming language and software environment for data analysis and graphics. 
*R* also has advanced capabilities for dealing with spatial, and spatio-temporal, data; and it provides unparalleled opportunities for analyzing such data. 


Tutorial
--------

.. topic:: 1. `Introduction to R <intr>`__

  Start here if you have never used *R*, or if you need a refresher. 
  

.. topic:: 2. `Spatial data manipulation with R <spatial>`__

  Read this to learn about the basics of reading, writing, and manipulating spatial data.

  
.. topic:: 3. `Spatial data analysis <analysis>`__

  A introduction to methods for description, prediction and inference with spatial data.
  

.. topic:: 4. `Case studies <cases>`__

   A (small) collection of case studies that can help you learn more about particular topics and design your own workflows. 
   

   
   
Special topics
--------------

.. topic:: I. `Spherical computation <sphere>`__ 

   Computing distances and other measures on a sphere or spheroid.
   

.. topic:: II. `Species Distribution Modeling <sdm>`__ with R

   A in-depth tutorial for predicting the geographic ranges of species.


.. topic:: III. Spatial simulation

   Spatial simulation models with R.  

   
 
   
   

.. This is the table of contents, must be here, but can be hidden so we can format how we like above.

.. toctree::
   :maxdepth: 2
   :hidden:

   intr/index.rst
   spatial/index.rst
   analysis/index.rst
   cases/index.rst
   sphere/index.rst
   sdm/index.rst

   