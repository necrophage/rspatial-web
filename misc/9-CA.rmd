# 9 A simple cellular automaton

```{r, echo=FALSE}
library(knitr)
opts_chunk$set(
  fig.width  = 5,
  fig.height = 5,
  fig.cap = '',
  collapse   = TRUE
)
```


Game of life cellular automaton
```{r}
gameOfLife <- function(x) {
  w <- matrix(c(1,1,1,1,0,1,1,1,1), nr=3,nc=3)
	nb <- focal(x, w=w, pad=TRUE, padValue=0)
	# Any live cell with fewer than two live neighbours dies, as if caused by under-population.
	# Any live cell with more than three live neighbours dies, as if by overcrowding.
	x[nb<2 | nb>3] <- 0
	# Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.	
	x[x==0 & nb==3] <- 1
	x
}
```


Initial conditions
```{r}
library(raster)
init <- raster(matrix(0, nrow = 40, ncol = 40))
init[18:22,18:22] <- 1

library(animation)
ani.options(interval = 0.05)
```


Function to run the model
```{r}
sim <- function(x, fun, n=100) {
	for (i in 1:n) {
		x <- fun(x)
		plot(x, legend=FALSE, asp=NA, main=i)
		ani.pause()  ## pause for a while ('interval')
	}
	invisible(x)
}
```


Now simulate. I am only showing 9 steps, you should try more steps!
```{r, ca4, fig.width=7.5, fig.height=7.5}
par(mfrow=c(3,3))
x <- sim(init, gameOfLife, n=9)
```


Gosper glider gun. Again showing 1 step only.

```{r, ca6, fig.width=7.5, fig.height=7.5}
gunInit <- function() {
	m <- matrix(0, nc=48, nr=34)
	m[2, 26] <- 1
	m[3, c(24,26)] <- 1
	m[4, c(14:15, 22:23, 36:37)] <- 1
	m[5, c(13,17, 22:23, 36:37)] <- 1
	m[6, c(2:3, 12, 18, 22:23)] <- 1
	m[7, c(2:3, 12, 16, 18:19, 24, 26)] <- 1
	m[8, c(12, 18, 26)] <- 1
	m[9, c(13, 17)] <- 1
	m[10, c(14:15)] <- 1
	raster(m)
}

r <- gunInit()
par(mfrow=c(3,3))
sim(r, gameOfLife, n=9)
```


*Question. Make a simple city growth CA-model for central California, using (some of) the data below*

```{r, ca8}
alt <- raster('alt.grd')
slope <- terrain(alt, 'slope')

pslope = 1 - slope*2
pslope <- reclassify(pslope, c(-10, 0.85, NA))
plot(pslope)

sea <- reclassify(alt, c(NA, NA, 1, -1000, 10000, NA))
dsea <- distance(sea, doEdge=T)
dsea[is.na(alt)] <- NA
pdissea <- 1 - dsea / maxValue(dsea)
pdissea <- exp(pdissea)
pdissea <- pdissea / maxValue(pdissea)

city <- alt * 0
w <- matrix(1, nr=3,nc=3)/9
```


Very simple city growth model
```{r, cacity}

cityLife <- function(x) {
	w <- matrix(c(1,1,1,1,0,1,1,1,1), nr=3,nc=3)
	nb <- focal(x, w=w, pad=TRUE, padValue=0, na.rm=TRUE)
	x[x==0 & nb > 0] <- 1
	x
}
```

```{r, ca10}
# plot(city)
# click(city, cell=TRUE)
## I clicked near Oakland
##     cells layer
##[1,] 36165     0

city1 <- city
city1[36165] <- 1

sim(city1, cityLife, n=1)
```

Now, can you

1. add simple slope constraint (no exapansion on steep slopes)

2. add new cities (e.g. by seeding randomly, but perhaps based on some additional conditions)

3. Add something else?


